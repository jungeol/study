class FourCal:
    def __init__(self, a, b):
        self.setData(a, b)

    def setData(self, a, b):
        self.a = a
        self.b = b
    def sum(self):
        return self.a + self.b
    
    def mul(self):
        return self.a * self.b
    
    def sub(self):
        return self.a - self.b
    
    def div(self):
        return self.a / self.b


if(__name__ == '__main__'):
    fc = FourCal(1,2)
    print('%s : %s, %s, %s, %s' % (type(fc), fc.sum(), fc.mul(), fc.sub(), fc.div()))