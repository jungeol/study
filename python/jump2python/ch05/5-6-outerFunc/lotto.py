import random

class Lotto:
    def __getNumber(self):
        ret = []
        for i in range(5):
            n = random.randint(1,45)
            ret.append(n)
        return ret
    def getNumbers(self, n):
        ret = []
        for i in range(n):
            ret.append(self.__getNumber())
        return ret

if __name__ == '__main__':
    l = Lotto()
    print(l.getNumbers(5))