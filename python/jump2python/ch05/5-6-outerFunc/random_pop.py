import random
def random_pop(data):
    number = random.randint(0, len(data)-1)
    return data.pop(number)

def random_pop2(data):
    number = random.choice(data)
    data.remove(number)
    return number

def random_pop3(data):
    data = random.shuffle(data)
    return data.pop(0)

if __name__ == '__main__':
    data = list(range(1,46))
    while data: print(random_pop(data))