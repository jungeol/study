class HousePark:
    lastname = '박'
    def __init__(self, name):
        self.fullname = self.lastname + name
    def travel(self, where):
        print("%s, %s여행을 가다." % (self.fullname, where))
    def love(self, other):
        print("%s, %s와 사랑에 빠졌네" % (self.fullname, other.fullname))
    def fight(self, other):
        print("%s, %s와 싸우네" % (self.fullname, other.fullname))
    def __add__(self, other):
        print("%s, %s와 결혼했네." % (self.fullname, other.fullname))
    def __sub__(self, other):
        print("%s, %s와 이혼했네" % (self.fullname, other.fullname))

class HouseKim(HousePark):
    lastname = '김'
    def travel(self, where, day):
        print("%s, %s여행을 %s가네." % (self.fullname, where, day))

pey = HousePark('응용')
juliet = HouseKim('줄리엣')
pey.travel('부산')
juliet.travel('부산', 3)
juliet.love(pey)
pey + juliet
pey.fight(juliet)
pey - juliet