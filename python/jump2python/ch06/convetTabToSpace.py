import sys

src = sys.argv[1]
dst = sys.argv[2]

#print(src)
#print(dst)

f = open(src)
space_content = f.read()
f.close()

tab_content = space_content.replace(' ', '\t')
f = open(dst, 'w')
f.write(tab_content)
f.close()