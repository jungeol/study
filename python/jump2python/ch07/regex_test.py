import re

data = '''
park 000000-0000000
kim 111111-1111111
'''

pat = re.compile("(\d{6})[-]\d{7}")
print(pat.sub('\g<1>-*******', data))