import http.client

try:
    BODY = "***filecontents***"

    conn = http.client.HTTPConnection("localhost", 8888)
    conn.request("PUT", "/file", BODY)
    response = conn.getresponse()

    print(response.status, response.reason)
except ConnectionRefusedError as e:
    print(e)