import urllib.request

# 쿠키 핸들러 생성, 쿠키 데이터 처리는 디폴트로 CookieJar 객체를 사용함
cookie_handler = urllib.request.HTTPCookieProcessor()

opener = urllib.request.build_opener(cookie_handler)
urllib.request.install_opener(opener)
# 쿠키 데이터와 함께 서버로 요청
u = urllib.request.urlopen("http://www.example.com/login.html")