import http.client

conn = http.client.HTTPConnection("www.example.com")
conn.request("HEAD", "/index.html")
res = conn.getresponse()
print(res.status, res.reason)
data = res.read()
print(len(data))