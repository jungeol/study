import urllib.request

proxy_handler = urllib.request.ProxyHandler({'http': 'http://www.example.com:3128'})
proxy_auth_handler = urllib.request.ProxyBasicAuthHandler()
proxy_auth_handler.add_password('realm', 'host', 'uesrname', 'passwd')

opener = urllib.request.build_opener(proxy_handler, proxy_auth_handler)
# install_opener(), urlopen() 함수 대신에 직접 open 함수를 사용할 수도 있음
u = opener.open("http://www.example.com/login.html")