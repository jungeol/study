import urllib.request, urllib.parse, http.client

params = urllib.parse.urlencode({
    "@number": 12524, "@type": "issue", "@action": "show"
})
headers = {"Content-Type": "application/x-www-formurlencoded", "Accept": "text/plain"}
conn = http.client.HTTPConnection('bugs.python.org')
conn.request("POST", "", params, headers)
response = conn.getresponse()
print(response.status, response.reason)

data = response.read()
print(len(data))
