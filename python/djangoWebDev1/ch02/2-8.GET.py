import http.client

conn = http.client.HTTPConnection("www.example.com")
conn.request("GET", "/index.html")
r1 = conn.getresponse()
print(r1.status, r1.reason) #r1.msg : 응답 헤더

data1 = r1.read()
#print(data1)

conn.request("GET", "/parrot.spam") #request(method, url, body, header)
r2 = conn.getresponse()
print(r2.status, r2.reason)

data2 = r2.read()
#print(data2)

conn.close()