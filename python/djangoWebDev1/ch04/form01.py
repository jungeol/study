from django.shortcuts import render
from django.http import HttpResponseRedirect

def get_name(request):
    #POST 방식이면, 데이터가 담긴 제출된 폼으로 간주합니다.
    if request.method == 'POST':
        # request에 담긴 데이터로, 클래스 폼을 생성합니다.
        form = NameForm(request.POST)
        # 폼에 담긴 데이터가 유효한지 체크합니다.
        if form.is_valid():
            #폼 데이터가 유효하면, 데이터는 cleaned_data로 복사됩니다.
            new_name = form.cleaned_data['name']
            #로직에 따라 추가적인 처리를 합니다.
            
            #새로운 URL로 리다이렉션 시킵니다.
            return HttpResponseRedirect('/thanks/')
    #POST 방식이 아니면 (보통은 GET 요청임)
    #빈 폼을 사용자에게 보여줍니다.
    else:
        from = NameForm()
    
    return render(request, 'name.html', {'form': form})