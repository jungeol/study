# ch03에서 python3 manage.py shell 으로 명령어 실행

# 모델 임포트
from polls.models import Question, Choice

q = Question.objects.all()
c = Choice.objects.all()

#추가 레코드 생성
from django.utils import timezone
q = Question(question_text="What's up", pub_date=timezone.now())
q.save()
print(q.id)

#속성에 접근
print(q.question_text)
#속성값 수정
q.question_text = 'What\'s new?'
q.save()

#테이블의 모든 레코드 조회
Question.objects.all()

