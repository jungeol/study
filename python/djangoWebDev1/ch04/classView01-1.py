# urls.py
from django.conf.urls import patterns
from myapp.views import MyView

urlpatterns = patterns('', (
    (r'^about/', MyView.as_view())
))