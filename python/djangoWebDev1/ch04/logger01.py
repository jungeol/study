# 파이썬의 logging 모듈을 임포트
import logging

# 로거 인스턴스를 취득함
logger = logging.getLogger(__name__)

def my_view(request, arg1, argN):
    #필요한 로직
    if bad_mojo:
        # ERROR 레벨 이사의 로그 레코드를 기록함
        logger.error('Something went worng!')
