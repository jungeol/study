from polls.models import Question, Choice

#필터 사용
Question.objects.filter(id=1)
Question.objects.filter(question_text__startswith="what")

#올해 생성된 질문
from django.utils import timezone
current_year = timezone.now().year
Question.objects.get(pub_date__year=current_year)

## 1:N 관계, 외래키 관련
#choice_set() API사용
q = Question.objects.get(pk=1)
chSet = q.choice_set
chSet.all()
chSet.count()

# __로 객체 간의 관계를 표현
current_year = timezone.now().year
Choice.objects.filter(question__pub_date__year=current_year)

#삭제
q.choice_set.filter(choice_text__startswith='Reading')
q.delete()

