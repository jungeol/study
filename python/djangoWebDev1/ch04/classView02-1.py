# views.py

from django.http import HttpResponse

def my_view(request):
    if request.method == 'GET':
        #뷰 로직 작성
        return HttpResponse('result')

from django.views.generic import View

class MyView(View):
    def get(self, request):
        # 뷰 로직 작성
        return HttpResponse('result')