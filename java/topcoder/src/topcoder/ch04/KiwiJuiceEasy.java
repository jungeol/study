package topcoder.ch04;

public class KiwiJuiceEasy {
	public static void main(String[] args){
		KiwiJuiceEasy kj = new KiwiJuiceEasy();
		
		int[] capacities = new int[]{30,20,10};
		int[] bottles = new int[]{10,5,5};
		int[] formId = new int[]{0,1,2};
		int[] toId = new int[]{1,2,0};
		
		int[] result = kj.thePouring2(capacities, bottles, formId, toId);
		
		for(int n : result){
			System.out.print(n+",");
		}
	}
	public int[] thePouring(int[] capacities, int[] bottles, int[] formId, int[] toId){		
		for(int i=0; i<capacities.length; ++i){
			int f = formId[i];
			int t = toId[i];
			
			int vol = Math.min(bottles[f], capacities[t]-bottles[t]);
			
			bottles[f] -= vol;
			bottles[t] += vol;
		}
		return bottles;
	}
	
	public int[] thePouring2(int[] capacities, int[] bottles, int[] fromId, int[] toId){
		for(int i=0; i<fromId.length; ++i){
			int sum = bottles[fromId[i]] + bottles[toId[i]];
			bottles[toId[i]] = Math.min(sum, capacities[toId[i]]);
			bottles[fromId[i]] = sum - bottles[toId[i]];
		}
		return bottles;
	}
}
