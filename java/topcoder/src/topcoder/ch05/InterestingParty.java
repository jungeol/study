package topcoder.ch05;

import java.util.*;

public class InterestingParty {
	
	public static void main(String[] args){
		InterestingParty ip = new InterestingParty();
		
		String[] first = new String[] {"A", "B", "C"};
		String[] second = new String[] {"B", "C", "B"};
		
		System.out.println(ip.bestInvitation(first, second));
	}
	
	public int bestInvitation(String[] first, String[] second){
		HashMap<String, Integer> hm = new HashMap<String, Integer>();
		
		
		for(String s : first){
			if(!hm.containsKey(s))	hm.put(s, 1);
			else hm.put(s, hm.get(s)+1);
		}
		for(String s : second){
			if(!hm.containsKey(s))	hm.put(s, 1);
			else hm.put(s, hm.get(s)+1);
		}
		
		
		int max = 0;
		for(String key : hm.keySet()){
			if(max<hm.get(key)) max=hm.get(key);
		}
		return max;
	}
}
