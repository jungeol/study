package jump2java;

import java.util.ArrayList;
import java.util.HashMap;

public class Test {
	public static void main(String[] args){
		StringBuffer sb = new StringBuffer();
		sb.append("hello");
		sb.append(" ");
		sb.append("World!!");
		System.out.println(sb.toString());
		
		int[] odds = {1, 3, 5, 7, 9};
		System.out.println(odds[0]);
		
		ArrayList<String> pitches = new ArrayList<String>();
		pitches.add("1111");
		pitches.add("2222");
		pitches.add("3333");
		pitches.add("4444");
		System.out.println(pitches.get(1));
		System.out.println(pitches.get(0));
		
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("p", "It's P!");
		map.put("s", "2");
		System.out.println(map.get("p"));
		
		String[] numbers = {"one", "two", "three"};
		for(String number : numbers){
			System.out.println(number);
		}
	}
}
