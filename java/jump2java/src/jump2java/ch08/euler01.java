package jump2java.ch08;

public class euler01 {
	static int getMultiSum(int mul){
		int result = 0;
		
		for(int i=1; i<=1000; i++){
			if(i%3==0 || i%5==0){
				result += i;
			}
		}
		
		return result;
	}
	public static void main(String[] args){
		System.out.println("Multiples of 3 and 5 : "+(getMultiSum(3)+getMultiSum(5)));
	}
}
