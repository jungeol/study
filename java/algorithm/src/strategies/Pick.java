package strategies;

import java.util.ArrayList;

public class Pick {
	
	public static void main(String[] args){
		Pick p = new Pick();
		p.pick(10, new ArrayList<Integer>(), 10);
	}
	
	void pick(int n, ArrayList<Integer> picked, int toPick){
		if(toPick == 0){
			System.out.println(picked); return;
		}
		
		int smallest = picked.size();
		
		for(int next=smallest; next<n; ++next){
			picked.add(next);
			pick(n, picked, toPick-1);
			picked.remove(picked.size()-1);
		}
	}
}
